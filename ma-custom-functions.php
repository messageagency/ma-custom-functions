<?php
/**
 * Plugin Name: MA Custom Functions
 * Plugin URI: http://www.messageagency.com
 * Description: MA CPTs, ACF Blocks, and other functions not linked to the theme
 * Version: 1.0
 * Author: Message Agency
 * Author URI: http://www.messageagency.com
 */

// ACF Functions
   // Options Page
       if( function_exists('acf_add_options_page') ) {
         acf_add_options_page(array(
           'page_title' 	=> 'Theme General Settings',
           'menu_title'	=> 'Theme Settings',
           'menu_slug' 	=> 'theme-general-settings',
           'capability'	=> 'edit_posts',
           'redirect'		=> false
         ));

         acf_add_options_sub_page(array(
           'page_title' 	=> 'Theme Header Settings',
           'menu_title'	=> 'Header',
           'parent_slug'	=> 'theme-general-settings',
         ));

         acf_add_options_sub_page(array(
           'page_title' 	=> 'Theme Footer Settings',
           'menu_title'	=> 'Footer',
           'parent_slug'	=> 'theme-general-settings',
         ));

         acf_add_options_sub_page(array(
           'page_title' 	=> 'Features',
           'menu_title'	=> 'Features',
           'parent_slug'	=> 'theme-general-settings',
         ));

       }
       add_filter( 'timber_context', 'wpsprout_timber_context'  );

       function wpsprout_timber_context( $context ) {
           $context['options'] = get_fields('option');
           return $context;
       }

   // ACF save point folder
   // https://www.advancedcustomfields.com/resources/local-json/
       add_filter('acf/settings/save_json', 'my_acf_json_save_point');
         function my_acf_json_save_point( $path ) {
             // update path
             $path = get_stylesheet_directory() . '/acf-json';
             // return
             return $path;
         }
         function generate_options_css() {
             $ss_dir = get_stylesheet_directory();
             ob_start();
             require($ss_dir . '/src/inc/styles.php');
             $css = ob_get_clean();
             file_put_contents($ss_dir . '/src/components/00-base/01-variables/_php-styles.scss', $css, LOCK_EX);
         }
       add_action( 'acf/save_post', 'generate_options_css', 20 );

   // Custom ACF MA blocks -- custom functions
       add_action('acf/init', 'my_acf_init_block_types');
       function my_acf_init_block_types() {

         // MA Billboard block
         if( function_exists('acf_register_block_type') ) {
             acf_register_block_type(array(
                 'name'              => 'billboard',
                 'title'             => __('Billboard Band'),
                 'description'       => __('A custom billboard block.'),
                 'render_template'   => 'blocks/ma-billboard/ma-billboard.php',
                 'category'          => 'design',
                 'icon'              => 'cover-image',
                 'keywords'          => array( 'billboard', 'ma', 'message agency' ),
                 'mode' 							=> 'edit',
                 'enqueue_style' 		=> get_template_directory_uri() . '/dist/css/style.css',
             ));
         }

         // MA Story block
         if( function_exists('acf_register_block_type') ) {
             acf_register_block_type(array(
                 'name'              => 'story',
                 'title'             => __('Story Band'),
                 'description'       => __('A custom story block.'),
                 'render_template'   => 'blocks/ma-story/ma-story.php',
                 'category'          => 'design',
                 'icon'              => 'align-pull-left',
                 'keywords'          => array( 'story', 'ma', 'message agency' ),
                 'mode' 							=> 'edit',
             ));
         }

         // MA Signpost block
         if( function_exists('acf_register_block_type') ) {
             acf_register_block_type(array(
                 'name'              => 'signpost',
                 'title'             => __('Signpost Band'),
                 'description'       => __('A custom signpost block.'),
                 'render_template'   => 'blocks/ma-signpost/ma-signpost.php',
                 'category'          => 'design',
                 'icon'              => 'ellipsis',
                 'keywords'          => array( 'signpost', 'ma', 'message agency' ),
                 'mode' 							=> 'edit',
             ));
         }

         // MA Latest News block
         if( function_exists('acf_register_block_type') ) {
             acf_register_block_type(array(
                 'name'              => 'latest-news',
                 'title'             => __('Latest News Grid Band'),
                 'description'       => __('A custom lastest news block.'),
                 'render_template'   => 'blocks/ma-latest-news/ma-latest-news.php',
                 'category'          => 'design',
                 'icon'              => 'screenoptions',
                 'keywords'          => array( 'latest', 'news', 'ma', 'message agency' ),
                 'mode' 							=> 'edit',
             ));
         }

         // MA Menu Grid block
         if( function_exists('acf_register_block_type') ) {
             acf_register_block_type(array(
                 'name'              => 'menu-grid',
                 'title'             => __('Menu Grid Band'),
                 'description'       => __('A menu-grid block.'),
                 'render_template'   => 'blocks/ma-menu-grid/ma-menu-grid.php',
                 'category'          => 'design',
                 'icon'              => 'screenoptions',
                 'keywords'          => array( 'menu', 'grid', 'ma', 'message agency' ),
                 'mode' 							=> 'edit',
             ));
         }

         // MA Accordion block
         if( function_exists('acf_register_block_type') ) {
             acf_register_block_type(array(
                 'name'              => 'accordion',
                 'title'             => __('Accordion Band'),
                 'description'       => __('A custom accordion block.'),
                 'render_template'   => 'blocks/ma-accordion/ma-accordion.php',
                 'category'          => 'design',
                 'icon'              => 'admin-comments',
                 'keywords'          => array( 'accordion', 'ma', 'message agency' ),
         				'mode' 							=> 'edit',
             ));
         }

         // MA Overview block
         if( function_exists('acf_register_block_type') ) {
             acf_register_block_type(array(
                 'name'              => 'overview',
                 'title'             => __('Title and Overview Band'),
                 'description'       => __('A custom title and overview block.'),
                 'render_template'   => 'blocks/ma-overview/ma-overview.php',
                 'category'          => 'design',
                 'icon'              => 'admin-comments',
                 'keywords'          => array( 'body', 'text', 'title', 'overview', 'ma', 'message agency' ),
         				'mode' 							=> 'edit',
             ));
         }
     }

     // Allow only custom blocks - Reminder: add all new MA blocks
         function ma_allowed_block_types( $allowed_blocks, $post ) {

           $allowed_blocks = array(
             'acf/billboard',
             'acf/story',
             'acf/signpost',
             'acf/latest-news',
             'acf/menu-grid'
           );

           $theme_features = get_field('theme_features', 'option');

           if( $post->post_type === 'page' ) {
             $allowed_blocks[] = 'core/shortcode';
           }

           if( $theme_features['accordion'] ) {
             $allowed_blocks[] = 'acf/accordion';
           }

           if( $theme_features['overview'] ) {
             $allowed_blocks[] = 'acf/overview';
           }

           return $allowed_blocks;

         }

      add_filter( 'allowed_block_types', 'ma_allowed_block_types', 10, 2 );

// Custom Post Types
// All CPT routes for pagination are located in functions.php
   // Generic CPT
       function custom_post_generic() {
         $labels = array(
           'name'               => _x( 'Generic Page', 'post type general name' ),
           'singular_name'      => _x( 'Generic Page', 'post type singular name' ),
           'add_new'            => _x( 'Add New', 'book' ),
           'add_new_item'       => __( 'Add New Generic Page' ),
           'edit_item'          => __( 'Edit Generic Page' ),
           'new_item'           => __( 'New Generic Page' ),
           'all_items'          => __( 'All Generic Pages' ),
           'view_item'          => __( 'View Generic Page' ),
           'search_items'       => __( 'Search Generic Pages' ),
           'not_found'          => __( 'No generic pages found' ),
           'not_found_in_trash' => __( 'No generic pages found in the Trash' ),
           'menu_name'          => 'Generic Page'
         );
         $args = array(
           'labels'        => $labels,
           'description'   => 'Holds our generic pages and generic page specific data',
           'public'        => true,
           'supports'      => array( 'title' ),
           'has_archive'   => false,
           'menu_icon'			=> 'dashicons-welcome-widgets-menus',
         );
         register_post_type( 'generic', $args );
       }
       add_action( 'init', 'custom_post_generic' );

       function add_author_support_to_posts() {
           add_post_type_support( 'generic', 'author' );
        }
        add_action( 'init', 'add_author_support_to_posts' );

     // News CPT
         function custom_post_news() {
           $labels = array(
             'name'               => _x( 'News', 'post type general name' ),
             'singular_name'      => _x( 'News', 'post type singular name' ),
             'add_new'            => _x( 'Add New', 'book' ),
             'add_new_item'       => __( 'Add New News' ),
             'edit_item'          => __( 'Edit News' ),
             'new_item'           => __( 'New News' ),
             'all_items'          => __( 'All News' ),
             'view_item'          => __( 'View News' ),
             'search_items'       => __( 'Search News' ),
             'not_found'          => __( 'No news posts found' ),
             'not_found_in_trash' => __( 'No news posts found in the Trash' ),
             'menu_name'          => 'News'
           );
           $args = array(
             'labels'        => $labels,
             'description'   => 'Holds our news pages and news page specific data',
             'public'        => true,
             'supports'      => array( 'title' ),
             'has_archive'   => true,
             'menu_icon'			=> 'dashicons-text-page',
             'taxonomies' => array('post_tag'),
           );
           register_post_type( 'news', $args );
         }
         add_action( 'init', 'custom_post_news' );

         add_action( 'pre_get_posts', 'sprout_pre_get_posts' );
         function sprout_pre_get_posts( $query )
         {
             if ( ! $query->is_main_query() || $query->is_admin() )
                 return false;
             if ( $query->is_tag() ) {
                 $query->set( 'post_type', 'news' );
                 $query->set( 'posts_per_page', 10 );
             }
             return $query;
         }

         // News CPT Admin Page Add Featured Img
           add_filter( 'manage_news_posts_columns', 'ma_cpt_filter_news_posts_columns' );
            function ma_cpt_filter_news_posts_columns( $columns ) {
              $columns['news_featured_image'] = __( 'Featured Image' );
              $columns['title'] = __( 'Article Name' );
              $columns['date'] = __( 'Date' );
              return $columns;
            }

            add_filter( 'manage_news_posts_columns', 'ma_cpt_news_columns' );
            function ma_cpt_news_columns( $columns ) {

                $columns = array(
                  'cb' => $columns['cb'],
                  'news_featured_image' => __( 'Featured Image' ),
                  'title' => __( 'Article Name' ),
                  'date' => __( 'Date', 'smashing' ),
                );

              return $columns;
            }

            add_action( 'manage_news_posts_custom_column', 'ma_cpt_news_column', 10, 2);
              function ma_cpt_news_column( $column, $post_id ) {
                if ( 'news_featured_image' === $column ) {
                  $image = get_field('news_featured_image');
                  // Thumbnail size attributes.
                  $size = 'thumbnail';
                  $thumb = $image['sizes'][ $size ];
                  $width = $image['sizes'][ $size . '-width' ];
                  $height = $image['sizes'][ $size . '-height' ];
                  ?><img src="<?php echo esc_url($thumb);; ?>" /> <?php
                }
              }

       // Event CPT
           function custom_post_events() {
             $labels = array(
               'name'               => _x( 'Events', 'post type general name' ),
               'singular_name'      => _x( 'Event', 'post type singular name' ),
               'add_new'            => _x( 'Add New', 'book' ),
               'add_new_item'       => __( 'Add New Event' ),
               'edit_item'          => __( 'Edit Event' ),
               'new_item'           => __( 'New Event' ),
               'all_items'          => __( 'All Events' ),
               'view_item'          => __( 'View Events' ),
               'search_items'       => __( 'Search Events' ),
               'not_found'          => __( 'No events found' ),
               'not_found_in_trash' => __( 'No events found in the Trash' ),
               'menu_name'          => 'Events'
             );
             $args = array(
               'labels'        => $labels,
               'description'   => 'Holds our events and news page specific data',
               'public'        => true,
               'supports'      => array( 'title' ),
               'has_archive'   => true,
               'menu_icon'			=> 'dashicons-calendar-alt',
             );
             register_post_type( 'events', $args );
           }
           add_action( 'init', 'custom_post_events' );

           // Event CPT Admin Page Add Featured Img, Event Date + Sort
             add_filter( 'manage_events_posts_columns', 'ma_cpt_filter_events_posts_columns' );
              function ma_cpt_filter_events_posts_columns( $columns ) {
                $columns['event_featured_image'] = __( 'Featured Image' );
                $columns['title'] = __( 'Event Name' );
                $columns['event_date'] = __( 'Event Date', 'smashing' );
                $columns['date'] = __( 'Date' );
                return $columns;
              }

              add_filter( 'manage_events_posts_columns', 'ma_cpt_events_columns' );
              function ma_cpt_events_columns( $columns ) {

                  $columns = array(
                    'cb' => $columns['cb'],
                    'event_featured_image' => __( 'Featured Image' ),
                    'title' => __( 'Event Name' ),
                    'event_date' => __( 'Event Date', 'smashing' ),
                    'date' => __( 'Date', 'smashing' ),
                  );

                return $columns;
              }

              add_action( 'manage_events_posts_custom_column', 'ma_cpt_events_column', 10, 2);
                function ma_cpt_events_column( $column, $post_id ) {
                  // Event Date column
                  if ( 'event_date' === $column ) {
                    echo get_field('event_date');
                  }

                  if ( 'event_featured_image' === $column ) {
                    $image = get_field('event_featured_image');
                    // Thumbnail size attributes.
                    $size = 'thumbnail';
                    $thumb = $image['sizes'][ $size ];
                    $width = $image['sizes'][ $size . '-width' ];
                    $height = $image['sizes'][ $size . '-height' ];
                    ?><img src="<?php echo esc_url($thumb);; ?>" /> <?php
                  }
                }

                add_filter( 'manage_edit-events_sortable_columns', 'ma_cpt_events_sortable_columns');
                  function ma_cpt_events_sortable_columns( $columns ) {
                    $columns['event_date'] = 'event_date';
                    return $columns;
                  }

                  add_action( 'pre_get_posts', 'ma_cpt_posts_orderby' );
                    function ma_cpt_posts_orderby( $query ) {
                      if( ! is_admin() || ! $query->is_main_query() ) {
                        return;
                      }

                      if ( 'event_date' === $query->get( 'orderby') ) {
                        $query->set( 'orderby', 'meta_value' );
                        $query->set( 'meta_key', 'event_date' );
                        $query->set( 'meta_type', 'date' );
                      }
                    }

 // Disable Core Functions
     // Disable Gutenberg on generic pages
     // https://www.billerickson.net/disabling-gutenberg-certain-templates/
       function ea_disable_editor( $id = false ) {
         $excluded_templates = array(
           'Generic Page'
         );
         $excluded_ids = array(
         );
         if( empty( $id ) )
           return false;
         $id = intval( $id );
         $template = get_page_template_slug( $id );
         return in_array( $id, $excluded_ids ) || in_array( $template, $excluded_templates );
       }

     // Disable Gutenberg by template
     // https://www.billerickson.net/disabling-gutenberg-certain-templates/
       function ea_disable_gutenberg( $can_edit, $post_type ) {

         if( ! ( is_admin() && !empty( $_GET['post'] ) ) )
           return $can_edit;

         if( ea_disable_editor( $_GET['post'] ) )
           $can_edit = false;

         return $can_edit;

       }
       add_filter( 'use_block_editor_for_post_type', 'ea_disable_gutenberg', 10, 2 );

     // Disable Classic Editor by template
     // https://www.billerickson.net/code/hide-editor-on-specific-page-template/
       function ea_disable_classic_editor() {

         $screen = get_current_screen();
         if( 'page' !== $screen->id || ! isset( $_GET['post']) )
           return;

         if( ea_disable_editor( $_GET['post'] ) ) {
           remove_post_type_support( 'page', 'editor' );
         }

       }
       add_action( 'admin_head', 'ea_disable_classic_editor' );

    // Reorder Admin Menu
      function custom_menu_order($menu_ord) {
        if (!$menu_ord) return true;
        return array(
         'index.php', // this represents the dashboard link
         'edit.php?post_type=page', // this is a custom post type menu
         'edit.php?post_type=generic',
         'edit.php?post_type=news',
         'edit.php?post_type=events',
         'edit.php', // this is the default POST admin menu
         'separator1', // First separator
         'upload.php', // Media
         'wpcf7', // Contact Form 7
         'cfdb7-list.php', // Contact Forms
         'separator2', // Second separator
         'themes.php', // Appearance
         'plugins.php', // Plugins
         'edit.php?post_type=acf-field-group', // ACF
         'theme-general-settings', // Theme settings
         'users.php', // Users
         'options-general.php', // Settings
         'tools.php', // Tools
         'separator3', // Third separator
         'ivory-search', // Ivory Search
         'gutenberg', // Gutenberg
         'advgb_main', // Advanged Gutenberg
         'font-awesome', // Font Awesome
        );
        }
        add_filter( 'custom_menu_order', 'custom_menu_order', 10, 1 );
        add_filter( 'menu_order', 'custom_menu_order', 10, 1 );


    // Remove html styling from iframe for accessibility -- custom function
        function ma_remove_width_attribute( $html ) {
           $html = preg_replace( '/(width|height)="\d*"\s/', "", $html );
           return $html;
        }
        add_filter('oembed_result', 'ma_remove_width_attribute');


        function my_login_logo() { ?>
              <style type="text/css">
                  #login h1 a, .login h1 a {
                    background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/src/images/logo.svg);
                		width: 100%;
                		background-size: 200px 115px;
                		background-repeat: no-repeat;
                  	padding-bottom: 30px;
                  }
              </style>
          <?php }
          add_action( 'login_enqueue_scripts', 'my_login_logo' );

          function my_login_logo_url() {
              return home_url();
          }
          add_filter( 'login_headerurl', 'my_login_logo_url' );

          function my_login_logo_url_title() {
              return 'MA Sprout';
          }
          add_filter( 'login_headertitle', 'my_login_logo_url_title' );


        add_role(
            'manager',
            __( 'Manager' ),
            array(
                'read'         => true,  // true allows this capability
                'edit_posts'   => true,
            )
        );

      // Personal admin color scheme
      // Use https://wpadmincolors.com/ to generate css file
        function ma_admin_admin_color_scheme() {
        //Get the theme directory
        $theme_dir = get_template_directory_uri();

        //MA Sprout
        wp_admin_css_color( 'ma-admin', __( 'WP Sprout Theme' ),
          $theme_dir . '/src/inc/ma-admin.css',
          array( '#4A4A4A', '#286da4', '#75BEE9', '#dbefff')
        );
        }
        add_action('admin_init', 'ma_admin_admin_color_scheme');

        // Custom Admin footer
          function wpexplorer_remove_footer_admin () {
          	echo '<span id="footer-thankyou">Built with love by <a href="http://www.messageagency.com/" target="_blank">Message Agency</a></span>';
          }
          add_filter( 'admin_footer_text', 'wpexplorer_remove_footer_admin' );


      // Remove dashboard widgets
        function remove_dashboard_meta() {
        	if ( ! current_user_can( 'manage_options' ) ) {
        		remove_meta_box( 'dashboard_primary', 'dashboard', 'normal' );
        		remove_meta_box( 'dashboard_quick_press', 'dashboard', 'side' );
        		remove_meta_box( 'dashboard_recent_comments', 'dashboard', 'normal' );
        	}
        }
        add_action( 'admin_init', 'remove_dashboard_meta' );

        function wpexplorer_remove_menus() {
          if ( ! current_user_can( 'manage_options' ) ) {
            // remove_menu_page( 'themes.php' );          // Appearance
          	remove_menu_page( 'plugins.php' );         // Plugins
          	// remove_menu_page( 'users.php' );           // Users
          	remove_menu_page( 'tools.php' );           // Tools
          	remove_menu_page( 'options-general.php' ); // Settings
          }
        }
        add_action( 'admin_menu', 'wpexplorer_remove_menus' );

        function wpexplorer_adjust_the_wp_menu() {
          $page = remove_submenu_page( 'themes.php', 'themes.php' );
          $page = remove_submenu_page( 'themes.php', 'theme-editor.php' );
          $page = remove_submenu_page( 'themes.php', 'gutenberg-widgets' );
          global $submenu;
          unset($submenu['themes.php'][6]); // Customize
        }
        add_action( 'admin_menu', 'wpexplorer_adjust_the_wp_menu', 999 );
